FROM microsoft/aspnetcore-build AS build

WORKDIR /app

COPY package.json ./
RUN npm install

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o out

FROM microsoft/aspnetcore:2

RUN apt-get update
RUN apt-get install nodejs-legacy -y

WORKDIR /app
COPY --from=build /app/out .
ENTRYPOINT ["dotnet", "AspNetCoreSpa.dll"]
